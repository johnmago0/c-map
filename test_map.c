#include "map.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define NTESTS 5
#define ARRAY_SIZE 1000

int *array_create(int size) {
  int *array = (int *)malloc(sizeof(int) * size);

  for (int i = 0; i < size; i++)
    array[i] = i;

  return array;
}

bool isIn(int *array, int size, int val) {
  for (int i = 0; i < size; i++) {
    if (val == array[i])
      return true;
  }
  return false;
}

/*----------------------------------------------------------------------------*/

bool test_insert(int *array, int size) {
  struct map_t *map = map_create(NULL);

  for (int i = 0; i < size; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    map_insert(map, i, x);
  }

  map_destroy(&map);
  return true;
}

bool test_search(int *array, int size) {
  struct map_t *map = map_create(NULL);

  for (int i = 0; i < size; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    map_insert(map, i, x);
  }

  for (int i = 0; i < size; i++) {
    int *y = (int *)map_search(map, i);
    if (y == NULL) {
      printf("(%d)Search in map failed\n", i);
      map_destroy(&map);
      return false;
    }
    if (*y != i) {
      printf("(%d)Value in map is not valid: %d != %d\n", i, *y, i);
      map_destroy(&map);
      return false;
    }
  }

  map_destroy(&map);
  return true;
}

bool test_remove(int *array, int size) {
  struct map_t *map = map_create(NULL);
  int *removeArray = array_create(size / 2);
  for (int i = 0, j = 0; i < size; i += 2, j++)
    removeArray[j] = array[i];

  for (int i = 0; i < size; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    map_insert(map, i, x);
  }

  for (int i = 0; i < size / 2; i++)
    map_remove(map, removeArray[i]);

  for (int i = 0, j = 0; i < size; i++, j += 2) {
    int *y = (int *)map_search(map, i);
    if (isIn(removeArray, size / 2, i) && y != NULL) {
      printf("(%d)Removal failed, value returned: %d\n", i, *y);
      map_destroy(&map);
      return false;
    }
    if (!isIn(removeArray, size / 2, i) && y == NULL) {
      printf("(%d)Removal failed, value not returned\n", i);
      map_destroy(&map);
      return false;
    }
  }

  free(removeArray);
  map_destroy(&map);
  return true;
}

bool test_remove_all(int *array, int size) {
  struct map_t *map = map_create(NULL);
  printf("Entry:\n");
  for (int i = 0; i < size; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    map_insert(map, i, x);
  }

  printf("Removal:\n");
  for (int i = 0; i < size; i++)
    map_remove(map, array[i]);

  for (int i = 0; i < size; i++) {
    int *y = (int *)map_search(map, i);
    if (y != NULL) {
      printf("(%d)Search in map failed, returned value even after removing it: "
             "%d\n",
             i, *y);
      map_destroy(&map);
      return false;
    }
  }
  map_destroy(&map);
  return true;
}

bool test_insert_and_remove(int *array, int size) {
  struct map_t *map = map_create(NULL);
  for (int i = 0; i < size; i++) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    map_insert(map, i, x);
  }

  for (int i = 0; i < size / 2; i++)
    map_remove(map, array[i]);

  for (int i = size / 2; i < size; i++) {
    int *y = (int *)map_search(map, i);
    if (y == NULL) {
      printf("(%d)Search in map failed\n", i);
      map_destroy(&map);
      return false;
    }
    if (*y != i) {
      printf("(%d)Value in map is not valid: %d != %d\n", i, *y, i);
      map_destroy(&map);
      return false;
    }
  }

  for (int i = (size / 2) - 1; i >= 0; i--) {
    int *x = (int *)malloc(sizeof(int));
    *x = i;
    map_insert(map, i, x);
  }

  for (int i = 0; i < size; i++) {
    int *y = (int *)map_search(map, i);
    if (y == NULL) {
      printf("(%d)Search in map failed\n", i);
      map_destroy(&map);
      return false;
    }
    if (*y != i) {
      printf("(%d)Value in map is not valid: %d != %d\n", i, *y, i);
      map_destroy(&map);
      return false;
    }
  }

  for (int i = 0; i < size; i++)
    map_remove(map, array[i]);

  for (int i = 0; i < size; i++) {
    int *y = (int *)map_search(map, i);
    if (y != NULL) {
      printf("(%d)Search in map failed, returned value after removing it: %d\n",
             i, *y);
      map_destroy(&map);
      return false;
    }
  }
  map_destroy(&map);
  return true;
}

/*----------------------------------------------------------------------------*/

int main() {
  int errs = NTESTS;
  int *array = array_create(ARRAY_SIZE);

  if (!test_insert(array, ARRAY_SIZE)) {
    printf("Insertion did not work\n");
    errs--;
  }

  if (!test_search(array, ARRAY_SIZE)) {
    printf("Search did not work\n");
    errs--;
  }

  if (!test_remove(array, ARRAY_SIZE)) {
    printf("Remove did not work\n");
    errs--;
  }

  if (!test_remove_all(array, ARRAY_SIZE)) {
    printf("Removing all values did not work\n");
    errs--;
  }

  if (!test_insert_and_remove(array, ARRAY_SIZE)) {
    printf("Insert and remove did not work\n");
    errs--;
  }

  free(array);
  printf("Tests %d/%d passed\n", errs, NTESTS);
}
