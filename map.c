/*  This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* For a better visualization, the following is recommended:
 * <https://www.cs.usfca.edu/~galles/visualization/ClosedHash.html> */

#include "map.h"

#include <assert.h>
#include <math.h>
#include <stdlib.h>

typedef struct map_t map_t;
typedef struct bucket_t bucket_t;
typedef enum bucket_state bucket_state;

#define LOAD_FACTOR 0.75

// Based on the hash algorithm:
// <http://www.cse.yorku.ca/~oz/hash.html#djb2>
#define hash_func(key) (5381 * 33 + (key))
#define load(map) ((double)(map)->entries / (double)(map)->size)

typedef enum resize {
  INCREASE,
  DECREASE,
} resize;

/*============================================================================*/
/*-----                            BUCKETS                               -----*/
/*============================================================================*/

static void bucket_remove(bucket_t *bucket, void (*del_function)(void *));

/* Create a bucket and initializes it's values. */
static bucket_t *bucket_create() {
  bucket_t *bucket = (bucket_t *)malloc(sizeof(bucket_t));
  bucket->state = EMPTY;
  bucket->data = NULL;
  return bucket;
}

/* Deallocate the values inside the bucket(including the bucket) and
 * set it to NULL. */
static void bucket_destroy(bucket_t **bucket, void (*del_function)(void *)) {
  bucket_remove(*bucket, del_function);
  free(*bucket);
  *bucket = NULL;
}

/* Deallocate the values inside the bucket and set it to be a TOMBSTONE */
static void bucket_remove(bucket_t *bucket, void (*del_function)(void *)) {
  if (bucket->data != NULL && del_function != NULL)
    del_function(bucket->data);
  else
    free(bucket->data);

  bucket->data = NULL;
  bucket->state = TOMBSTONE;
}

/*============================================================================*/
/*-----                              MAP                                 -----*/
/*============================================================================*/

/* Resize the hash map, and reinsert the values on the newly allocated buckets.
 * The load factor value and the formulas to make the resize can be found on the
 * following link:
 * <https://en.wikipedia.org/wiki/Hash_table#Overview> */
static void map_rehash(map_t *map, resize res_type) {
  uint64_t size = map->size;
  bucket_t **buckets = map->buckets;

  assert((res_type == INCREASE || res_type == DECREASE) &&
         "Resize type not allowed");
  if (res_type == INCREASE) {
    double x = ceil(map->size / LOAD_FACTOR);
    map->size = (uint64_t)x;
  } else {
    double x = floor(map->size * LOAD_FACTOR);
    map->size = (uint64_t)x;
  }

  map->buckets = (bucket_t **)malloc(sizeof(bucket_t *) * map->size);
  map->entries = 0;

  for (uint64_t i = 0; i < map->size; i++)
    map->buckets[i] = bucket_create();

  for (uint64_t i = 0; i < size; i++) {
    if (buckets[i]->state == FULL)
      map_insert(map, buckets[i]->key, buckets[i]->data);
    free(buckets[i]);
  }

  free(buckets);
}

struct map_t *map_create(void (*del_function)(void *)) {
  map_t *map = (map_t *)malloc(sizeof(map_t));
  map->entries = 0;
  map->size = MAP_INIT_SIZE;
  map->buckets = (bucket_t **)malloc(sizeof(bucket_t *) * map->size);
  map->del_function = del_function;
  for (uint64_t i = 0; i < map->size; i++)
    map->buckets[i] = bucket_create();
  return map;
}

void map_destroy(map_t **map) {
  for (uint64_t i = 0; i < (*map)->size; i++)
    bucket_destroy(&((*map)->buckets[i]), (*map)->del_function);

  free((*map)->buckets);
  free(*map);
  *map = NULL;
}

void *map_search(const map_t *map, keytype key) {
  uint64_t idx = hash_func(key) % map->size;
  do {
    switch (map->buckets[idx]->state) {
    case EMPTY:
      return NULL;
    case FULL:
      if (map->buckets[idx]->key == key)
        return map->buckets[idx]->data;
    case TOMBSTONE:
      idx = (idx + 1) % map->size;
      break;
    default:
      assert(0 && "Bucket state not valid");
    }
  } while (idx != (hash_func(key) % map->size));

  return NULL;
}

void map_insert(map_t *map, keytype key, void *data) {
  uint64_t idx = hash_func(key) % map->size;
  do {
    switch (map->buckets[idx]->state) {
    case EMPTY:
    case TOMBSTONE:
      map->buckets[idx]->data = data;
      map->buckets[idx]->key = key;
      map->buckets[idx]->state = FULL;
      map->entries++;
      if (load(map) >= LOAD_FACTOR)
        map_rehash(map, INCREASE);
      return;
    case FULL:
      if (map->buckets[idx]->key == key)
        return;
      idx = (idx + 1) % map->size;
      break;
    default:
      assert(0 && "Bucket state not valid");
    }

    if (idx == (hash_func(key) % map->size))
      map_rehash(map, INCREASE);
  } while (true);
}

void map_remove(map_t *map, keytype key) {
  uint64_t idx = hash_func(key) % map->size;

  do {
    switch (map->buckets[idx]->state) {
    case FULL:
      if (map->buckets[idx]->key == key) {
        bucket_remove(map->buckets[idx], map->del_function);
        map->entries--;
        if (load(map) < (LOAD_FACTOR / 4) && map->size != MAP_INIT_SIZE) {
          map_rehash(map, DECREASE);
          return;
        }
      }
    case TOMBSTONE:
      idx = (idx + 1) % map->size;
      break;
    case EMPTY:
      return;
    default:
      assert(0 && "Bucket state not valid");
    }

  } while (idx == (hash_func(key) % map->size));
}
