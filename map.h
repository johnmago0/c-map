/*  This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAP_H_
#define MAP_H_

#include <stdbool.h>
#include <stdint.h>

typedef uint32_t keytype;

#define MAP_INIT_SIZE 41

enum bucket_state {
  FULL,
  EMPTY,
  TOMBSTONE,
};

struct bucket_t {
  keytype key;
  void *data;
  enum bucket_state state;
};

/* NOTE: All deallocation of the data inserted in the map is responsability of
 * it, you should not deallocate data inside the map without using one of */
/* its functions for it. */
struct map_t {
  struct bucket_t **buckets;
  uint64_t entries;
  uint64_t size;
  void (*del_function)(void *);
};

/* Create a map and initializes it's values.
 *
 * @del:
 *  Function pointer to delete the data stored in the buckets.
 *  If the function value is NULL, a simple free() call will be used. */
struct map_t *map_create(void (*del_function)(void *));

/* Deallocate all the values inside the map(including the map) and
 * set it to NULL.
 * NOTE: This function deallocate all the data stored on the buckets. */
void map_destroy(struct map_t **);

/* Search the tree by the key. */
void *map_search(const struct map_t *, keytype);

/* Inserts a value on the map.
 * NOTE: Duplicate values will not be inserted on the map. */
void map_insert(struct map_t *, keytype, void *);

/* Remove data stored in the map.
 * NOTE: This function uses the del_function passed in the @map_create to
 * deallocate the value asked to be removed. */
void map_remove(struct map_t *, keytype);

#endif // MAP_H_
